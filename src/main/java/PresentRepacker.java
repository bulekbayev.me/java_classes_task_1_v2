import java.util.*;

public class PresentRepacker {

    public static void sortPresentBySugar(Present present) {
        System.out.println("Sorting present's content...");
        if (present.getCandies().isEmpty()) {
            System.out.println("Your present is empty!");
        } else {
            ArrayList<Candy> tempList = new ArrayList<>();
            tempList.addAll(present.getCandies());
            Comparator<Candy> comparator = Comparator.comparing(obj -> obj.getSugarContent());
            Collections.sort(tempList, comparator);
            Set<Candy> sortedSet = new LinkedHashSet<>();
            sortedSet.addAll(tempList);
            present.setCandies(sortedSet);
            System.out.println("Done!");
        }
    }

    public static void sortPresentByWeight(Present present) {
        System.out.println("Sorting present's content...");
        if (present.getCandies().isEmpty()) {
            System.out.println("Your present is empty!");
        } else {
            ArrayList<Candy> tempList = new ArrayList<>();
            tempList.addAll(present.getCandies());
            Comparator<Candy> comparator = Comparator.comparing(obj -> obj.getWeight());
            Collections.sort(tempList, comparator);
            Set<Candy> sortedSet = new LinkedHashSet<>();
            sortedSet.addAll(tempList);
            present.setCandies(sortedSet);
            System.out.println("Done!");
        }
    }

    public static void findCandyInRangeOfSugarContent(Present present, double sugarContentMinimumValue, double sugarContentMaximumValue) {
        System.out.println("Looking for candy...");
        if (present.getCandies().isEmpty()) {
            System.out.println("Your present is empty!");
        } else {
            for (Candy tempSweet : present.getCandies()) {
                if (tempSweet.getSugarContent() >= sugarContentMinimumValue && tempSweet.getSugarContent() <= sugarContentMaximumValue) {
                    System.out.println(tempSweet.toString() + " " + tempSweet.getSugarContent() + "% is in range of sugar content.");
                }
            }
        }
    }
}
