import java.util.*;

public class Present {

    private double weight;
    private double price;
    private Set<Candy> candies;

    public Present() {
        this.candies = new HashSet<Candy>();
    }

    public void addCandyToPresent(Candy candy) {
        if (isCandyInThePresent(candy) == false) {
            candies.add(candy);
            this.weight += candy.getWeight();
            this.price += candy.getPrice();
        } else {
            System.out.println("You already have " + candy.getName() + " in the present!");
        }
    }

    private boolean isCandyInThePresent(Candy candy) {
        boolean isCandyInside = false;
        for (Candy tempCandy : candies) {
            if (tempCandy.getName().equals(candy.getName())) {
                isCandyInside = true;
            }
        }
        return isCandyInside;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<Candy> getCandies() {
        return candies;
    }

    public void setCandies(Set<Candy> candies) {
        this.candies = candies;
    }

    @Override
    public String toString() {
        return "Present: " + candies.toString();
    }
}
