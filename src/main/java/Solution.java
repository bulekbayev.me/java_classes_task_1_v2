public class Solution {

    public static void main(String[] args) {
        Present present = new Present();
        present.addCandyToPresent(new Candy("Snickers", 220, 1260, 50));
        present.addCandyToPresent(new Candy("Twix", 180, 285, 70));
        present.addCandyToPresent(new Candy("Mars", 210, 290, 85));
        present.addCandyToPresent(new Candy("Albeni", 75, 210, 80));
        present.addCandyToPresent(new Candy("Oreo", 120, 340, 75));
        present.addCandyToPresent(new Candy("Albeni", 75, 210, 80));
        present.addCandyToPresent(new Candy("Twix", 180, 285, 70));
        present.addCandyToPresent(new Candy("Alyonka", 225, 239, 60));
        System.out.println("Present's weight is " + present.getWeight());
        System.out.println("Present's price is " + present.getPrice());
        System.out.println(present.toString());
        PresentRepacker.sortPresentBySugar(present);
        System.out.println(present.toString());
        PresentRepacker.findCandyInRangeOfSugarContent(present, 50, 70);
    }
}
