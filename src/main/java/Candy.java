import java.util.Objects;

public class Candy {

    private String name;
    private double weight;
    private double price;
    private int sugarContent;

    public Candy(String name, double weight, double price, int sugarContent) {
        this.name = name;
        this.weight = validateWeight(weight);
        this.price = validatePrice(price);
        this.sugarContent = validateSugarContent(sugarContent);
    }

    private double validateWeight(double weight) {
        if (weight > 0) {
            return weight;
        } else {
            throw new NumberFormatException("Weight must be greater than 0!");
        }
    }

    private double validatePrice(double price) {
        if (price > 0) {
            return price;
        } else {
            throw new NumberFormatException("Price must be greater than 0!");
        }
    }

    private int validateSugarContent(int sugarContent) {
        if (sugarContent >= 0 || sugarContent <= 100) {
            return sugarContent;
        } else {
            throw new NumberFormatException("Sugar content must be in range of 0 to 100!");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSugarContent() {
        return sugarContent;
    }

    public void setSugarContent(int sugarContent) {
        this.sugarContent = sugarContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return Objects.equals(name, candy.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
